<?php


class Tarea extends Conexion
{
    public $id;
    public $tarea;
    public $fecha;
    public $descipcion;

//funcion que te muestra todos os registros en la bse de datos
    static function mostrar(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT *FROM tareas");
        $pre -> execute();
        $res = $pre -> get_result();

        $tareas = [];
        while ($tarea = $res ->fetch_object(Tarea::class)){
            //Devuelve los elementos
            array_push($tareas,$tarea);
        }
        return $tareas;
    }
    //Funcion que te ayuda a saber que tareas ya van a vencer
    static function vencimiento(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT id FROM tareas where fecha between curdate() and date_add(curdate(), interval 5 day)");
        $pre -> execute();
        $res = $pre -> get_result();

        $tareasVencidas = [];
        while ($tareasV = $res ->fetch_object(Tarea::class)){
            //Devuelve los elementos
            array_push($tareasVencidas,$tareasV);
        }
        return $tareasVencidas;
    }
//funcion para buscar un tareas por medio de id
    static function buscarId($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me -> con, "SELECT *FROM tareas WHERE id =?");
        $pre -> bind_param("i", $id);
        $pre -> execute();
        $res = $pre -> get_result();

        return $res -> fetch_object(Tarea::class);
    }

//funcion para insertar los tareas en la bse de datos
    function insertar(){
        $pre = mysqli_prepare($this->con, "INSERT INTO tareas (tarea,fecha,descripcion) VALUES (?,?,?)");
        $pre -> bind_param("sss", $this -> tarea, $this -> fecha, $this -> descripcion);
        $pre -> execute();
        $pre= mysqli_prepare($this->con, "SELECT LAST id");
    }
//funcion para actualizar las tareas
    function update(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "UPDATE tareas SET  tarea=?, fecha=?, descripcion=? WHERE id=?");
        $pre -> bind_param("sssi", $this -> tarea, $this -> fecha, $this -> descripcion, $this->id);
        $pre ->execute();
        return true;
    }
//funcion para remover una tarea
    static function remove ($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "DELETE FROM tareas WHERE id=?");
        $pre-> bind_param("i",$id);
        $pre -> execute();
        return true;
    }



}